package com.osf.translate.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.osf.translate.dao.TranslationDAO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@CrossOrigin("*")
public class TranslationController {
	
	@Resource
	private TranslationDAO tsdao;
	
	@CrossOrigin("*")
	@RequestMapping(value = "/translations", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getTranslationsRank() {
		List<Map<String, Object>> tempList = tsdao.selectListForRank();
		for (Map<String, Object> tempMap : tempList) {
			if(tempMap.get("TH_RESPONSE") instanceof Clob) {
				tempMap.put("TH_RESPONSE", clobToString((Clob)tempMap.get("TH_RESPONSE")));
			}
		}
		log.info("rankk list =>{}",tempList);
		return tempList;
	}
	
	@GetMapping("/translation/{source}/{target}/{text}")
	public @ResponseBody Map<String, Object> doTranslation(
			@PathVariable("target") String target,
			@PathVariable("source") String source,
			@PathVariable("text") String text
			){
		log.info("target=>{},source=>{},text=>{}",
				new String[] {target,source,text});
		Map<String, String> param = new HashMap<String, String>();
		param.put("target", target);
		param.put("source", source);
		param.put("text", text);
		Map<String, Object> rMap = tsdao.selectTranslationHisOne(param);
		if(rMap==null) {
			rMap = translationTest(param);
			if(rMap.get("TH_ERROR_CODE")!=null) {
				param.put("errCode", rMap.get("TH_ERROR_CODE").toString());
				param.put("result", "");
			} else {
				param.put("errCode", "");
				rMap = (Map<String, Object>)rMap.get("message");
				rMap = (Map<String, Object>)rMap.get("result");
				String result = rMap.get("translatedText").toString();
				param.put("result", result);
			}
			
			tsdao.insertTranslationHisOne(param);
			rMap = tsdao.selectTranslationHisOne(param);
		} else {
			tsdao.updateTranslationHisCount(rMap);
		}
		if(rMap.get("TH_RESPONSE") instanceof Clob) {
			rMap.put("TH_RESPONSE", clobToString((Clob)rMap.get("TH_RESPONSE")));
		}
		return rMap;
	}

	private Map<String, Object> translationTest(Map<String, String> param) {
		String clientId = "2rM1Ok5nO0jDfjV026WL";
		String clientSecret = "as8_S7WU9k";
		String text =param.get("text");
		String source = param.get("source");
		String target = param.get("target");
		
		try {
			text = URLEncoder.encode(text, "UTF-8");
			String apiURL = "https://openapi.naver.com/v1/papago/n2mt";
			URL url = new URL(apiURL);
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("X-Naver-Client-Id", clientId);
			con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
			String postParams = "source" + source + "&target=" + target + "&text=" + text;
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(postParams);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			BufferedReader br;
			if(responseCode == 200) {
				br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			} else {
				br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
			}
			ObjectMapper om = new ObjectMapper();
			br.close();
			Map<String, Object> rMap = om.readValue(response.toString(), Map.class);
			if(rMap.get("errorCode") != null) {
				
			}
			log.info("rMap =>{}", rMap);
			return rMap;
		} catch (Exception e) {
			log.error("error => {}", e);
		}
		return null;
	}
	
	private String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);
			String line;
			while(null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			
		} catch (IOException e) {
			
		}
		return sb.toString();
	}
}
