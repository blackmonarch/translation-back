package com.osf.translate.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;

import org.springframework.stereotype.Repository;

@Repository
public class TranslationDAO {
	
	@Resource
	private SqlSession ss;
	
	public List<Map<String, Object>> selectTranslationList(){
		return ss.selectList("com.osf.translate.mapper.TranslationMapper.selectList");
	}
	
	public Map <String, Object> selectTranslationHisOne(Map<String, String>param){
		return ss.selectOne("com.osf.translate.mapper.TranslationMapper.selectOne",param);
	}
	
	public Integer insertTranslationHisOne(Map<String, String> param) {
		return ss.insert("com.osf.translate.mapper.TranslationMapper.insertOne", param);
	}
	
	public Integer updateTranslationHisCount(Map<String, Object> param) {
		return ss.update("com.osf.translate.mapper.TranslationMapper.updateCount", param);
	}
	
	public List<Map<String, Object>> selectListForRank(){
		return ss.selectList("com.osf.translate.mapper.TranslationMapper.selectListForRank");
	}


}
