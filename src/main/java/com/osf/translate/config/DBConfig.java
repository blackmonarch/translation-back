package com.osf.translate.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.osf.translate.mapper")
public class DBConfig {

}
